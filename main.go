package main

import (
	"bufio"
	"fmt"
	"mangtastest1/models"
	"mangtastest1/service"
	"os"
)

func main() {
	// returns a reader having a buffer of default size
	input := bufio.NewReader(os.Stdin)
	// output to user
	fmt.Println("Enter the string: ")
	// reads input from user
	text, _ := input.ReadString('\n')
	// copies input to struct
	inputRequest := models.InputRequest{Input: text}
	// calls the function that performs operation, if there is an error, it returns an error
	result, err := service.GetInputFile(inputRequest, false)
	if err != nil {
		fmt.Println(err)
	}
	// output result to user
	fmt.Println(result)
}
