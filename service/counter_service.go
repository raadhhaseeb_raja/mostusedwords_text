package service

import (
	"errors"
	"mangtastest1/models"
)

func GetInputFile(inputString models.InputRequest, excludeStopWords bool) (mostUsedWordsWithOccurrence *[]models.SortedKeyVal, err error) {
	// check if input is valid
	isValid := inputString.Validate()
	// if not valid, returns custom response with an error
	if !isValid {
		return nil, errors.New("invalid string")
	}

	// input is converted to lowercase for easier ops
	inputString.Input = models.ToLower(inputString.Input)

	// if stop words require excluding, they are excluded from the input
	if excludeStopWords {
		inputString.Input = models.RemoveStopWords(inputString.Input)
	}
	// CountSubStrings counts the substrings by the occurrences and stores
	// each element/substring as a key and also saves occurrences as value
	wordsWithOccurrences := models.CountSubStrings(inputString.Input)
	// SortAndCut sorts the map[string]int by adding them to a struct and
	// then appends the struct to another struct that only takes in the top 10 sorted values by descending order
	sortedWordsWithOccurrences := models.SortAndCut(wordsWithOccurrences)
	// returns sorted data
	return &sortedWordsWithOccurrences, nil
}
