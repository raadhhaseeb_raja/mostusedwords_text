package models

import "strings"

// Validate :
// Validates the input by checking if the input file is empty, if it is empty, we return a false,
// we return true if it is not empty
func (i InputRequest) Validate() bool {
	if len(strings.TrimSpace(i.Input)) == 0 {
		return false
	} else {
		return true
	}
}
