package models

///////////////////////////////////////////////Input
// User input
type InputRequest struct {
	Input string `json:"input"`
}

// StoryStatsPack packs the story in a single struct
type StoryStatsPack struct {
	TenMostUsedWordsWithOccurrence string `json:"tenMostUsedWordsWithOccurrence"`
}

type KeyVal struct {
	Key   string
	Value int
}

type SortedKeyVal struct {
	Key string `json:"value,omitempty"`
	Val int    `json:"occurrence,omitempty"`
}
